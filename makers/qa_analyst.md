QA Analyst
--------

### Should have
* Understanding of SDLC and how QA is a part of it
* Experienced working on a team or in a group academic setting
* Tested software and created defects
* Reproduced Customer-reported issues
* A thirst for knowledge and continuous learning
* A connection to OnlineMedEd’s mission and values

### Is expected to
* Communicate well with all members of the team
* Learn quickly to understand product under test
* Perform tasks assigned to them after initial instructions
* Bring issues to other team members or management
* Actively pursue improving QA skills, including but not limited to problem-solving & writing test cases 
* Demonstrate the ability to learn from mistakes
* Begin the process of tying stories back to user value & impact for our members & organizers
