Software Developer in Test (SDET)
--------

### Should have
* Responsibilities include both software development and testing tasks and focus on the testability, robustness, and performance of production software
* Excellent time management and organizational skills
* Understanding of BDD coding and tools (Cucumber, Hiptest, etc)
* Know various test method & corresponding tools
* Understanding of Object-Oriented Design
* Ability to keep current with the constantly changing technology industry


### Is expected to
* Setting up, maintaining, and performing test automation frameworks on multiple application platforms, such as Mobile, Desktop, and Web, and building test scenarios and acceptance tests.
* Participating in design and architectural discussion
* Ensuring extensive unit test code coverage
* Being capable of building, deploying and managing own environment (customizing when required)
* Developing quality automation code and maintaining the same
* Checking for product scalability
* Find bottlenecks and thresholds in existing code with the help of automation tools.

