OnlineMedEds focus is to make better doctors by providing the best information and learning experience on the market. We improve learning by delivering personalized and adaptive experiences for users and institutions. Part education, part tech, we have strong traction in every medical school throughout the country and 200k+ MAUs in 191 countries worldwide.
We are looking for a full-time Staff Software Quality Engineer to join our team. You will help to ensure the quality of our products while working in a great environment. Do you have the ability to test with a user-centric mindset?  Can you utilize your expertise and knowledge to help drive QA best practices and process improvements? Are you passionate about driving quality through the company? Then you may be the right candidate for us. 
Skills:

•	7-10 years of professional experience in quality assurance and leading quality efforts within an agile environment team of 2-6 QE engineers, including international teams.
•	Advanced working knowledge and ability to use tools (such as IDE, debugger, build tools, source control, ServiceNow instances, profilers, system administration/Unix tools) to assist with daily tasks
•	5+ years creating and executing Service / API tests or in automation disciplines 
•	Skilled in root cause analysis, results analysis, and performance analysis  
•	Strong architecture knowledge, technical solution design, and vendor management experience
•	Experience with testing databases and all layers of the application technology stack
•	Experienced in training junior members of the team and ensuring high quality of deliverables from them
•	Ability to write complex SQL Queries to validate back end data
•	Desire to continue learning new methods and tools to better themselves and the team
Expectations: 
•	Participating in design reviews on development projects representing quality assurance and the customer
•	Anticipate and mitigate test and project issues
•	A connection and adhesion to OnlineMedEd's mission and values 
•	Able to find opportunities for improvement and tackle them without external direction
•	Be a problem solver and be able to handle multiple and changing priorities
•	Have excellent communication and presentation skills
•	Lead medium to large-scale projects
•	Assist other QA team members to understand and scope work
•	Participate in planning to point out issues and work with owners on acceptance criteria
•	Recognize opportunities for enhancements and continuous improvements and present proposals
•	Stay on top of current industry software testing tools and methodologies and make substantial contributions to developing and deploying new test technologies, tools, or methods
•	Become subject matter expert (SME) for all areas of product
•	Step up to handle escalated issues and emergency testing


