QA Engineer
--------

### Should have
* 2 or more years of experience in Software testing
* Worked on an agile software development team in a QA role
* Validated back end data using SQL
* Created test plans / cases at the story level
* Some exposure to services and how to test them

### Is expected to
* Be a problem solver and be able to handle multiple and changing priorities
* Work with product owners to establish acceptance tests
* Assist in maintaining automation suites
* Understand all functions of product
* Bring solutions to team and management when problems are encountered
* Actively pursue helping team achieve its goals 
* Show creativity and initiative to improve product coverage and effectiveness
* Create and maintain documentation for quality assurance procedures

