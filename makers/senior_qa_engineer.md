Senior QA Engineer
--------

### Should have
* Five or more years of experience in Software testing
* Been the go-to QA person on an agile software development team
* Written complex SQL Queries to validated back end data
* Solid communication and presentation skills
* Created test plans for new functionality at the epic level
* Desire to continue learning new methods and tools to better themselves and the team

### Is expected to
* Assist other QA team members to understand and scope work
* Adjust team workload based on reprioritization from team
* Participate in planning to point out issues and work with owners on acceptance criteria
* Step up to handle escalated issues and emergency testing
* Interact daily with product owners to understand project scope and complexity
* Have a full understanding of automation and use it daily in their work
* Become subject matter expert (SME) for multiple areas of product
* Create solutions and act upon them to resolve obstacles 
* Create and report key metrics to management 
* Train other QA engineers on the procedure and testing methodologies 
