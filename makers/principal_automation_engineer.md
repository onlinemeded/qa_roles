Principal Automation Engineer
--------

### Should have
* Strong working knowledge of AWS Cloud infrastructure (EC2, RDS, VPC peering, Route53, S3, Autoscaling) or hybrid environments (OpenStack)
* Good understanding of networking and related protocols
* Proficiency with source control, continuous integration
* Exposure to messaging pub/subsystems (e.g., RabbitMQ, Active-MQ, Kinesis, Kafka, etc.)
* Experience with Release Management processes and controls 

### Is expected to
* Strong ability to architect development toolchains and infrastructure
* Troubleshoot critical development systems (Build failures, critical web services)
* Planning and Developing CI/CD Pipeline Automation
* Support ongoing innovation in the platform and automation space
* Provide training sessions and teach skills that lead towards organization-wide adoption of automation practices

