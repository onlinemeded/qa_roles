This repo contains a list of Quality Assurance titles, roles, and associated responsibilities:

### Makers

[Makers path](makers/makers_path.md)

* [QA Analyst](makers/qa_analyst.md)
* [QA Engineer](makers/qa_engineer.md)
* [Senior QA Engineer](makers/senior_qa_engineer.md)
* [Principal QA Engineer](makers/principal_qa_engineer.md)
* [QA Architect ](makers/qa_architect.md)
* [Automation Engineer](makers/automation_engineer.md)
* [Senior Automation Engineer](makers/senior_automation_engineer.md)
* [SDET (Software Developer in Test)](makers/sdet.md)
* [Principal Automation Engineer](makers/principal_automation_engineer.md)
* [Automation Architect ](makers/automation_architect.md)


### Managers

[Managers path](managers/managers_path.md)

* [Lead QA Engineer](managers/lead_qa_engineer.md)
* [QA Manager](managers/qa_manager.md)
* [QA Director](managers/qa_director.md)
* [VP, Quality Assurance](managers/vp_quality_assurance.md)

### Roles

* [Product Engineering Lead](product_engineering_lead.md)

