Automation Architect
--------

### Should have
* Strong ability to architect development toolchains and infrastructure
* Strong knowledge of and background on Automation frameworks, Tools and Practices
* Substantial experience with configuration management, monitoring and systems tool
* Experience working with cloud-based technologies
* Deep understanding of technology trends and broad knowledge of technology products and vendors
* Knowledge of scripting languages such as Java, JavaScript, Perl, Ruby, Python, PHP, Groovy, Bash
* At least ten years of Hands-on experience in writing Automation Tests

### Is expected to
* Build robust automated logging, monitoring, and alerting systems
* Design new technology solutions that meet technical, security, and business needs for apps/workloads
* Contribute to and maintain capacity planning, performance analysis, develop expansion and growth plans, lead efforts towards optimization
* Be able to prepare and present recommendations to senior staff
* Have a deep understanding of how CI/CD tools and ability to develop custom plugins
