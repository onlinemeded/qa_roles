Makers focus on: software testing architecture to improve customer experience
--------

### Architecture & automation frameworks & software testing
* Principals & Architects build out testing and automation frameworks with QA engineers
* Principals are decision makers on adoption of architecture into stack and frameworks
* All engineers: hands-on architecture and testing within the SDLCC

### Best practices & architecture & code reviews
* Principals ensure framework reviews happen across teams
* Principals enforce best practices and ensure SOP's are documented, followed, and maintained
* All engineers: ensure review of framework and tests happenand that best practices are documented, followed, and maintained

### Technical skills evaluation & mentoring
* More senior QA engineers mentor and grow other engineers' technical skills
* Support managers in technical career development plans for engineers


### Introduce new testing and automation tools & mentor adoption
* Introduce new tools and technologies that improve quality and speed to teams
* Evangelize and teach QA engineers about new tools
* Partner with managers on adoption of new tools


### Recommend process improvements & support adoption
* Recommend process improvements that improve quality and speed to managers and teams
* Partner with managers on adoption of process improvements

### Career Paths
![Career Paths](../qa_career_path.png "Career Paths")