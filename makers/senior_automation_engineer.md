Senior QA Automation Engineer
--------

### Should have
* Five or more years of experience in automation of software testing
* Designed or helped design automation frameworks
* Demonstrated expertise utilizing high-quality automation to solve test problems
* Strong knowledge of at least one programming language such as Python, Ruby, PERL
* Experience with testing methodologies such as BDD, TDD, regression, automation, exploratory testing, etc
* Extensive knowledge around Build/Deploy pre & post processes
 
### Is expected to
* Writing automation scripts and implementing software applications
* Promote the use of test automation across teams and educate all stakeholders to the benefits of integrated automated testing
* Have experience with BDD test automation frameworks using Cucumber or Selenium
* Prioritize and perform multiple tasks simultaneously
* Participates in reviews and inspections of test automation designs and code
* Have had extensive experience in testing backend API with SOAPUI or similar toolset
