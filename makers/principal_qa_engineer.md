Principal QA Engineer
--------

### Should have
* More than seven years of experience in Software testing in a very technical role
* Planned and implemented QA testing efforts on major projects
* Excellent communication and presentation skills to all levels of management
* Research, recommend and implement new tools into the process
* History of successfully meeting commitments

### Is expected to
* Work with QA management on improvement to overall QA workflow
* Point of contact for QA testing methodology
* Understand and ensure compliance with policies, procedures, and laws governing our industry/business and products
* Work with other non-IT teams to find ways that QA can help those teams.
* Interact daily with product owners to understand project scope and complexity
* Represent the Voice of the customer to development teams 
* Become subject matter expert (SME) for the entire product
* Implement continuous process improvements to enhance testing efficiency 
