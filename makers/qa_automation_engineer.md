QA Automation Engineer
--------

### Should have
* Four to five years of QA experience with two-plus years of experience in automation testing
* Worked on an agile software development team in a QA role
* Designed and executed automation tests into an existing framework
* Work closely with other teams to develop test strategies and scenarios for new and existing products
* Have hands-on experience with automation tools and scripting
* Familiarity with at least one programming language such as Python, Ruby, PERL
* Strong collaboration skills to work with the team to gather necessary information
 
### Is expected to
* Troubleshoot automation software and finalizing system procedures
* Created test plans around automation schedule
* Help team test products when a schedule is tight
* Writing automation scripts and implementing software applications
* Execute test cases on multiple environments and track results 
