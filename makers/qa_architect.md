QA Architect
--------

### Should have
* A passion for QA
* Nine or more years of experience in Software testing
* Substantial experience in programming, design, and analysis
* Extensive hands-on technical expertise coupled with ingenuity 
* Proven experience "owning" projects in whole from the technical perspective
* Ability to engineer enterprise-class test architecture
* Successfully set up and implemented CI/CD in conjunction with other IT leaders
* Experience in identifying the root cause of issues and developing solutions



### Is expected to
* Participate in planning sessions with Architecture teams
* Champion QA across the enterprise 
* Establish standards and guidelines, system/subsystem definitions
* Work with management on improvement to the overall QA offering
* Understand and ensure compliance with policies, procedures, and laws governing our industry/business and products
* Analyze business/industry trends and formulate a plan to keep testing current
* Implement continuous process improvements to enhance testing efficiency
* Define testing quality metrics and continually improve them


