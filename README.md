This repo contains a list of QA titles, roles, and associated responsibilities:

### Makers

[Makers path](makers/makers_path.md)

#### QA Generalists
* [QA Analyst](makers/qa_analyst.md)
* [QA Engineer](makers/qa_engineer.md)
* [Sr. QA Engineer](makers/senior_qa_engineer.md)
* [Staff QA Engineer](makers/staff_qa_engineer.md)
* [Principal QA Engineer](makers/principal_qa_engineer.md)
* [QA Architect](makers/qa_architect.md)

#### QA Automation
* [Automation Engineer](makers/qa_automation_engineer.md)
* [SDET Sofware Development Engineer in Test](makers/sdet.md)
* [Sr. Automation Engineer](makers/senior_automation_engineer.md)
* [Principal Automation Engineer](makers/principal_automation_engineer.md)
* [Automation Architect](makers/automation_architect.md)

### Managers

[Managers path](managers/managers_path.md)

* [QA Lead](managers/lead_qa_engineer.md)
* [QA Manager](managers/qa_manager.md)
* [QA Director](managers/qa_director.md)
* [VP, Quality Assurance](managers/vp_quality_assurance.md)

