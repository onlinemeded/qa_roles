Lead QA Engineer
--------

### Should have
* Overseen several test projects simultaneously and provide scheduling support to operations
* Excellent written and verbal communications skills
* Ability to interact professionally and exhibit appropriate social skills
* Lead QA on the scrum team and helped with the estimation and assignments of tasks
* Served as a technical resource for Quality Assurance 
* Worked closely with development to increase the quality of products
* The ability to work within multi-functional teams

### Is expected to
* Identify training needs and organize train sessions for peers within QA.
* Responsible for implementing software testing methodologies to ensure the timely introduction of quality software.
* Develop timely and accurate software testing status reports and communicate that within the engineering team 
* Assist in breaking complex projects down into tasks and sub-tasks
* Establish and evolve formal QA processes, ensuring that the team is using industry-accepted best practices.
* Provide input into QA team performance reviews
* Mentor team members
