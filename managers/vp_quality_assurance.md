VP Quality Assurance
--------
### Should have
* 15+ years of QA experience with 10+ years of technical QA management experience
* Strong analytical ability, superb communication skills (verbal & written), standout colleague
* Worked closely with other business operations leaders to ensure alignment with the implementation of Quality processes 
* Demonstrated high-quality design skills and to understand overall conceptual design across integrated and multiple products
* Recommended adjustments, corrections or changes that improve and simplify the overall processing or accuracy of the products and reviewed steps for redundancies.



### Is expected to
* Identify opportunities to improve quality through tangible metric and increase operational efficiency
* Direct project prioritization and selection to maximize business and financial impact
* Lead the planning and execution of high-impact improvement projects and strategic initiatives
* Work with leaders to establish continuous improvement goals and act as a liaison with finance to build CI budgets and validating actual improvement results
* Develop a culture (and personally emulate the values) of inter-group teamwork and cooperation
* Create a world-class Quality Assurance team from internal resources (and by recruiting and hiring) a high-quality staff of associates



