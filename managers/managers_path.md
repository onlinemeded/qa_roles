Managers focus on: team management and development that drives results
--------

### Performance evaluation
* Regularly collect and provide feedback to QA engineers in 1:1s
* Regularly speak with engineering, product, and others about feedback from/about their team members

### Career growth
* Develop & regularly track goals and career growth plans for QA engineers in 1:1s
* Partner with QA engineering makers on specific technical or project goals and career growth plans

### Recruiting & resourcing
* Leadership of team recruiting and growth as well as partnership with talent/recruiting team
* Ensure QA engineers are assigned to right amount, variety, and type of work


### Rollout process improvements & adoption of engineering tools
* Introduce and rollout process improvements that improve quality and speed
* Recommend new testing and automation tools that improve quality and speed to makers and teams
* Partner with makers on adoption of process and tools


### Organization & administrative
* Leadership of team organization, skills development, and growth plans
* Remove obstacles for engineers and work collaboratively across teams
* Manage an efficient process for sharing and executing company mission and policy


### Career Paths
![Career Paths](../qa_career_path.png "Career Paths")