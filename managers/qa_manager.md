QA Manager
--------
### Should have
* Successfully monitored product quality and completed product, company, system, compliance, and surveillance audits
* Managed resources and project oversight through product launch phases where real-time troubleshooting and patch releases are required
* Demonstrated the ability to analyze, synthesize, and apply information to solve problems
* Reviewed escalated customer complaints and provided acceptable solutions to customer
* Must have collaborated with other members of management around Qa's decisions.
* Worked in ensuring the team has no single points of failure by review of the cross-training matrix


### Is expected to
* Manage the Quality Assurance team and provide supervision of all QA activities
* Handle QA human resource objectives including recruiting, training, assigning, scheduling, coaching, counseling, and disciplining employees
* Meets QA financial objectives including the preparation of an annual budget; scheduling expenditures; analyzing variances; initiating corrective actions
* React to changing goals and priorities within the company
* Prepare documentation and reports by collecting, analyzing and summarizing information and trends
* Create an environment of teamwork, open communication, ownership and accountability
* Serve as the Quality Advocate for IT


