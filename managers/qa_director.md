QA Director
--------
### Should have
* 5+ years experience supervising test and evaluation of technical efforts and team members
* Minimum of ten years of software quality assurance and testing experience
* MUST have proven experience designing, building, and implementing an SQA strategy from the ground up
* Recruited, led, managed, and mentored an SQA team
* Serve as the SQA lead on large and complex programs
* Been a collaborator with the ability to build relationships across the organization including with team, management, and development team
* Ability to coach and mentor staff, prioritizing changing needs and adapting as needed
* Understanding of SDLC and how QA is a part of it

### Is expected to
* Design, build and implement a software quality assurance (SQA) strategy from the ground-up
* Define SQA key performance indicators, standards, processes, procedures
* Actively participate and contribute to collaborative software solution design sessions
* Communicate well with all members of the team
* Provide timely and accurate status reporting of SQA tasks, risks, and issues to project stakeholders
* Implement risk management programs and deliver business value by aligning all functions on top company issues and risks.
* Responsible for creating an IT roadmap for Quality Systems, including implementation plans as well as scheduling upgrades and maintenance

